//
//  RestaurantesTVC.swift
//  PedidoOnLineiOS
//
//  Created by Orlando Amorim on 05/12/15.
//  Copyright © 2015 Orlando Amorim. All rights reserved.
//

import UIKit
import Parse

class RestaurantesTVC: UITableViewController, UISplitViewControllerDelegate{

    var backgroundImage : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Restaurantes"
        
        //Adição para funcionar melhor no iPad
        self.splitViewController!.delegate = self
        self.splitViewController!.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 20
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RestCell", forIndexPath: indexPath) as! RestaurantesTVCell

        cell.nomeLabel.text = "CocoBambu"
        cell.categoriaLabel.text = "Restaurante"
        cell.activityIndicator.stopAnimating()
        cell.logoImageView?.image = UIImage(named: "profile")
        cell.status.text = "Aberto"

        switch cell.status.text! {
        case "Aberto":
            cell.status.layer.borderColor = UIColor.greenColor().CGColor
            cell.status.textColor = UIColor.greenColor()
        default:
            cell.status.layer.borderColor = UIColor.redColor().CGColor
            cell.status.textColor = UIColor.redColor()
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("RestInfoSegue", sender: nil)
    }

    
    // MARK: - UISplitViewControllerDelegate
    //Adição para funcionar melhor no iPad
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool{
        return true
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }

    

}
