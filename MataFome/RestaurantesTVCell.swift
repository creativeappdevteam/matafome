//
//  RestaurantesTVCell.swift
//  PedidoOnLineiOS
//
//  Created by Orlando Amorim on 08/12/15.
//  Copyright © 2015 Orlando Amorim. All rights reserved.
//

import UIKit

class RestaurantesTVCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var categoriaLabel: UILabel!
    @IBOutlet weak var status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        logoImageView.layer.cornerRadius = 5
        logoImageView.layer.borderWidth = 2
        logoImageView.layer.borderColor = UIColor.grayColor().CGColor
        
        status.layer.cornerRadius = 5
        status.layer.borderWidth = 1

    }
}
