//
//  ViewController.swift
//  PedidoOnLineiOS
//
//  Created by Orlando Amorim on 15/12/15.
//  Copyright © 2015 Orlando Amorim. All rights reserved.
//



import UIKit
import Parse
import ParseUI
import SwiftyDrop

class ViewController: UIViewController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if (PFUser.currentUser() == nil) {
            let loginViewController = LoginViewController()
            loginViewController.delegate = self
            loginViewController.fields = [.UsernameAndPassword, .LogInButton, .PasswordForgotten, .SignUpButton, .Facebook, .Twitter]
//            loginViewController.emailAsUsername = true
//            loginViewController.signUpController?.emailAsUsername = true
            loginViewController.signUpController?.delegate = self
            self.presentViewController(loginViewController, animated: false, completion: nil)
        } else {
            presentLoggedInAlert()
        }
    }
    
    func logInViewController(logInController: PFLogInViewController, didLogInUser user: PFUser) {
        self.dismissViewControllerAnimated(true, completion: nil)
        presentLoggedInAlert()
    }
    
    func logInViewController(logInController: PFLogInViewController, didFailToLogInWithError error: NSError?) {
        if error != nil {
            let attributedString = NSAttributedString(string: "MataFome", attributes: [
                NSFontAttributeName : UIFont(name: "Pacifico", size: 40)!,
                NSForegroundColorAttributeName : UIColor.grayColor()
                ])

            let alertController = UIAlertController(title: "", message: "Erro ao logar, verifique os dados inseridos ou se voce possui um conexão com internet ativa.", preferredStyle: .Alert)
            alertController.setValue(attributedString, forKey: "attributedTitle")
            let OKAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
            alertController.addAction(OKAction)
            logInController.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func signUpViewController(signUpController: PFSignUpViewController, didFailToSignUpWithError error: NSError?) {
        if error != nil {
            let attributedString = NSAttributedString(string: "MataFome", attributes: [
                NSFontAttributeName : UIFont(name: "Pacifico", size: 40)!,
                NSForegroundColorAttributeName : UIColor.grayColor()
                ])
            
            let alertController = UIAlertController(title: "", message: "Erro ao criar conta, verifique os dados inseridos ou se voce possui um conexão com internet ativa.", preferredStyle: .Alert)
            alertController.setValue(attributedString, forKey: "attributedTitle")
            let OKAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
            alertController.addAction(OKAction)
            signUpController.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func signUpViewController(signUpController: PFSignUpViewController, didSignUpUser user: PFUser) {
        self.dismissViewControllerAnimated(true, completion: nil)
        presentLoggedInAlert()
    }
    
    func presentLoggedInAlert() {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.performSegueWithIdentifier("SegueApp", sender: nil)
    }
    
    func presentErrorMessage(){
        let alertController = UIAlertController(title: "You're logged in", message: "Welcome to Vay.K", preferredStyle: .Alert)
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.dismissViewControllerAnimated(true, completion: nil)
            self.performSegueWithIdentifier("SegueApp", sender: nil)
        }
        alertController.addAction(OKAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}

